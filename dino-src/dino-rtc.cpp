#include "dino5.h"
#include "dino5-error.h"
#include "dino-rtc.h"

extern DinoSerial ser;
extern I2C i2c;

DinoRtc::DinoRtc(RtcTime_t t)
{
    this->yr = t.yr;
    this->mo = t.mo;
    this->day = t.day;
    this->hr = t.hr;
    this->min = t.min;
    this->sec = t.sec;
    return;
}

DinoRtc::~DinoRtc(void)
{
    return;
}

static uint8_t dectohex(uint8_t dec) {
    uint8_t ones = dec % 10;
    uint8_t tens = (uint8_t)dec / (uint8_t)10;

    return (ones | (tens << 4));
}

Dino5Error_t DinoRtc::writeTimeToAm1805(uint16_t yr, uint8_t mo, uint8_t day, uint8_t hr, uint8_t min, uint8_t sec)
{
    uint8_t writebuf[6];
    writebuf[0] = dectohex(sec);
    writebuf[1] = dectohex(min);
    writebuf[2] = dectohex(hr);
    writebuf[3] = dectohex(day);
    writebuf[4] = dectohex(mo);
    writebuf[5] = dectohex(yr-2000);

    if (writeI2cStr(0xD2, 0x01, writebuf, 6))
    {
        return SUCCESS;
    }
    return FAIL;
}

Dino5Error_t DinoRtc::updateNrfTime(void)
{
    char timebuffer[10];
    if (readI2cStr(0xD2, 0x01, timebuffer, 6))
    {
        uint8_t tens = timebuffer[0];
        uint8_t ones = timebuffer[0];
        ones &= 0b00001111;
        tens &= 0b01110000;
        tens = tens >> 4;
        this->sec = ones + (tens * 10);

        tens = timebuffer[1];
        ones = timebuffer[1];
        ones &= 0b00001111;
        tens &= 0b01110000;
        tens = tens >> 4;
        this->min = ones + (tens * 10);

        tens = timebuffer[2];
        ones = timebuffer[2];
        ones &= 0b00001111;
        tens &= 0b00110000;
        tens = tens >> 4;
        this->hr = ones + (tens * 10);

        tens = timebuffer[3];
        ones = timebuffer[3];
        ones &= 0b00001111;
        tens &= 0b00110000;
        tens = tens >> 4;
        this->day = ones + (tens * 10);

        tens = timebuffer[4];
        ones = timebuffer[4];
        ones &= 0b00001111;
        tens &= 0b00010000;
        tens = tens >> 4;
        this->mo = ones + (tens * 10);

        tens = timebuffer[5];
        ones = timebuffer[5];
        ones &= 0b00001111;
        tens &= 0b11110000;
        tens = tens >> 4;
        this->yr = ones + (tens * 10) + 2000;

        return SUCCESS;
    }
    return FAIL;
}

static string datenumtostr(uint8_t n)
{
    if (n >= 10)
    {
        return to_string(n);
    }
    else
    {
        return string("0" + to_string(n));
    }
}

Dino5Error_t DinoRtc::getCdateFmt(string& cdate, RtcFmtType_t type)
{
    if (updateNrfTime() != SUCCESS)
    {
        return FAIL;
    }

    if (type == RTC_FMT_LOG)
    {
        cdate = "[" + datenumtostr(this->day) + this->motostr[this->mo] + \
        to_string(this->yr) + " " + datenumtostr(this->hr) + ":" + \
        datenumtostr(this->min) + ":" + datenumtostr(this->sec) + "]";

        return SUCCESS;
    }
    else if (type == RTC_FMT_DSEN)
    {
        cdate = to_string(this->yr) + "-" + datenumtostr(this->mo) + \
            "-" + datenumtostr(this->day) + "T" + datenumtostr(this->hr) + \
             ":" + datenumtostr(this->min) + ":" + datenumtostr(this->sec) + \
            ".000Z";

        return SUCCESS;
    }
    else
    {
        return FAIL;
    }
}