#ifndef DINO_RTC_H
#define DINO_RTC_H

#include "dino5.h"
#include "dino5-error.h"

/*
    dino-rtc.h

    Code dealing with the Real Time Clock. Mainly this class interfaces with the
    on board Ambiq Micro AM1805, but MBED classes are also leveraged. The general
    idea is to ensure that the AM1805 is carrying the correct time at all times.
    The AM1805 is always on -- even when the device is off. Mainly, the time is
    regulated by input from the GNSS receiver (EVA-M8Q), which carries exactly
    correct time continuously.
*/

typedef struct RtcTime_t
{
    uint8_t sec;
    uint8_t min;
    uint8_t hr;
    uint8_t day;
    uint8_t mo;
    uint16_t yr;
} RtcTime_t;

typedef enum RtcFmtType_e
{
    RTC_FMT_LOG,
    RTC_FMT_DSEN
} RtcFmtType_t;
// Custom type that informs the string formatter what type of formatting to use
// for a returned timestamp

class DinoRtc
{
    private:
        uint8_t sec;
        uint8_t min;
        uint8_t hr;
        uint8_t day;
        uint8_t mo;
        uint16_t yr;

        string motostr[13] = {"ERR", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", \
                "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

    public:
        DinoRtc(RtcTime_t t);
        ~DinoRtc();

        Dino5Error_t updateNrfTime(void);
        Dino5Error_t writeTimeToAm1805(uint16_t yr, uint8_t mo, uint8_t day, uint8_t hr, uint8_t min, uint8_t sec);
        Dino5Error_t getCdateFmt(string& cdate, RtcFmtType_t type);

        RtcTime_t defaulttime = {0,0,1,1,1,2000};
};

#endif