#include "dino-serial.h"
#include "dino5.h"
#include "mbed.h"

extern DigitalOut usb_select0, usb_select1;

DinoSerial::DinoSerial(PinName tx, PinName rx) : BufferedSerial(tx, rx) {
    return;
}

DinoSerial::~DinoSerial(void) {
    return;
}

Dino5Error_t DinoSerial::cavemanEvaResponseIntegrityCheck(const char* str, const int length) {
    if (length <= 4) return FAIL;
    if (str[0] != 0xB5) return FAIL;
    if (str[1] != 0x62) return FAIL;
    if ((str[length - 1] == 0) && (str[length - 2] == 0)) return FAIL;
    return SUCCESS;
}

Dino5Error_t DinoSerial::ez_strcmp(const char* str1, const char* str2, int len)
{
    int i;

    for(i=0;i<len;i++) if (str1[i] != str2[i]) return FAIL;

    return SUCCESS;
}

Dino5Error_t DinoSerial::sendEvaCmdAndGetReply(char* response, int expected_response_length, const char* cmd_to_send, int cmd_length, int ms_to_wait_for_reponse) 
{
    int i, j;

    wait(.01); //Required time spacing

    this->selectEvaSerial();
    
    for(i=0;i<cmd_length;i++) {
        this->putc(cmd_to_send[i]); // Send chars one at a time from the buffer to the Eva
        // Note, we can't do 'EvaSerial.printf(ubx_command_1)' because these commands contain NULLs (0x00). This will be
        // interpreted as 'This is the end of the string!' by printf, and stop sending after the first 0x00.
    }

    wait(ms_to_wait_for_reponse * .001); // Wait for response from EVA.

    for(j=0;j<expected_response_length;j++) {
        response[j] = this->getc(); // Copy the contents of the rx buffer to our local buffer;
    }

    return SUCCESS;//this->cavemanEvaResponseIntegrityCheck(response, expected_response_length);
}

Dino5Error_t DinoSerial::sendEvaCmdAndCheckReply(const char* expected_response, int expected_response_length, const char* cmd_to_send, int cmd_length, int ms_to_wait_for_reponse) 
{
    int i, j;
    char temp[MAX_UBX_REPLY_LENGTH];
    
    wait(.01); //Required time spacing
    
    this->selectEvaSerial();
    
    for(i=0;i<cmd_length;i++) {
        this->putc(cmd_to_send[i]); // Send chars one at a time from the buffer to the Eva
        // Note, we can't do 'EvaSerial.printf(ubx_command_1)' because these commands contain NULLs (0x00). This will be
        // interpreted as 'This is the end of the string!' by printf, and stop sending after the first 0x00.
    }

    wait(ms_to_wait_for_reponse * .001); // Wait for response from EVA.

    for(j=0;j<expected_response_length;j++) {
        temp[j] = this->getc(); // Copy the contents of the rx buffer to our local buffer;
    }

    return this->ez_strcmp(temp, expected_response, expected_response_length);
}

int DinoSerial::ezprintf(char* fmt, ...)
{
    wait(.05);
    va_list args;
    va_start(args, fmt);

    char temp[120];
    vsprintf(temp, fmt, args);
    puts(temp);

    va_end(args);
    wait(.05);

    return 1;
}

void DinoSerial::flushBuffer(void) {
    BufferedSerial::clear();
}

void DinoSerial::selectPcSerial(void) {
    usb_select0 = 0;
    usb_select1 = 0;
    this->flushBuffer();
    wait(.001);
}

void DinoSerial::selectEvaSerial(void) {
    usb_select0 = 1;
    usb_select1 = 0;
    this->flushBuffer();
    wait(.001);
}

void DinoSerial::selectEhs6Serial(void) {
    usb_select0 = 0;
    usb_select1 = 1;
    this->flushBuffer();
    wait(.001);
}

void DinoSerial::selectDebugSerial(void) {
    usb_select0 = 1;
    usb_select1 = 1;
    this->flushBuffer();
    wait(.001);
}