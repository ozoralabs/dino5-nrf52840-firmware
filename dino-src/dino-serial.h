#ifndef DINOSERIAL_H
#define DINOSERIAL_H

#include "dino5.h"
#include "dino5-error.h"

/*
    dino-serial.h

    Custom UART transceiver class that inherits BufferedSerial and RawSerial.
    Controls the UART multiplexer that selects the UART TX target. Also provides
    a way to flush the RX buffer that resides in the BufferedSerial parent 
    class. 
*/

#define MAX_UBX_REPLY_LENGTH        256

class DinoSerial : public BufferedSerial
{
    private:
        Dino5Error_t cavemanEvaResponseIntegrityCheck(const char* str, const int length);
        Dino5Error_t ez_strcmp(const char* str1, const char* str2, int len);

    public:
        DinoSerial(PinName tx, PinName rx);
        ~DinoSerial(void);

        void flushBuffer(void);

        Dino5Error_t sendEvaCmdAndGetReply(char* response, int expected_response_length, const char* cmd_to_send, int cmd_length, int ms_to_wait_for_reponse);
        Dino5Error_t sendEvaCmdAndCheckReply(const char* expected_response, int expected_response_length, const char* cmd_to_send, int cmd_length, int ms_to_wait_for_reponse);

        int ezprintf(char* fmt, ...);

        void selectPcSerial(void);
        void selectEvaSerial(void);
        void selectEhs6Serial(void);
        void selectDebugSerial(void);
};

#endif