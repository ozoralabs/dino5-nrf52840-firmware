#include "mbed.h"
#include "dino5.h"
#include "dino5-eeprom.h"

extern I2C i2c;

size_t NrfEeprom::writeByte(size_t wordAddress, char data){
    // Word Address/Data bytes in single array
    char command[2];
	command[0] = (wordAddress & 0xFF);
	command[1] = data;
    return i2c.write(EEPROM_ADDRESS, command, 2);

}

// Read single byte "data" from EEPROM at address "wordAddress"
size_t NrfEeprom::readByte(size_t wordAddress, char* data){
    // Word Address/Data bytes in single array
    char command = (wordAddress & 0xFF);
    // pointer to byte array for data to be read into
    char readData;
    // Perform random read by doing a dummy write in order to set EEPROM internal data word address counter
    if (i2c.write(EEPROM_ADDRESS, &command, 1, true) == 0) {
    size_t ret = i2c.read(EEPROM_ADDRESS, &readData, 1);
        if (ret > 0) {
            return ret;
        } else {
            *data = readData;
            return ret;
        }
    }
    return 1;
}

EepromError_e NrfEeprom::resetAquaInstructionPointer() {
    size_t writeRet = NrfEeprom::writeByte(0x4F, 0x50);
    if (writeRet == 0) {
        //sysmsg(LVL_EXTENDED, "EEPROM - Aqua instruction pointer (0x4F) reset to 0x50");
        return EEPROM_SUCCESS;
    } else {
        //sysmsg(LVL_ERROR, "EEPROM - i2c Error %d - Failed to reset aqua instruction pointer to 0x50", writeRet);
        return EEPROM_FAILURE;
    }
}

EepromError_e NrfEeprom::getNextInstructionAddress(char* nextAddy) {
    char data = 0;
    size_t readRet = NrfEeprom::readByte(0x4F, &data);
    if (readRet == 0) {
        //sysmsg(LVL_EXTENDED, "EEPROM - Instruction pointer = %d", data);
        *nextAddy = data;
        return EEPROM_SUCCESS;
    } else {
        //sysmsg(LVL_ERROR, "EEPROM - i2c Error %d - Failed to read instruction pointer (0x4F)", readRet);
        return EEPROM_FAILURE;
    }
}

EepromError_e NrfEeprom::getStartMode(char* mode) {
    char data = 0;
    size_t readRet = NrfEeprom::readByte(0x40, &data);
    if (readRet == 0) {
        if (data == 0) {
            *mode = data;
            //sysmsg(LVL_EXTENDED, "EEPROM - Start Mode = Continuous Interval");
            return EEPROM_SUCCESS;
        } else if (data == 1) {
            //sysmsg(LVL_EXTENDED, "EEPROM - Start Mode = GMT Start Mode");
            *mode = data;
            return EEPROM_SUCCESS;
        } else {
            //sysmsg(LVL_ERROR, "EEPROM - Start Mode read out of bounds; got %d", data);
            return EEPROM_FAILURE;
        }
    } else {
        //sysmsg(LVL_ERROR, "EEPROM - i2c Error %d - Failed to read instruction pointer (0x4F)", readRet);
        return EEPROM_FAILURE;
    }
}

EepromError_e NrfEeprom::getNextInstruction(char* nextInstructionCode) {
    char nextInstructionAddy = 0, data = 0;
    if (getNextInstructionAddress(&nextInstructionAddy) == EEPROM_SUCCESS) {
        size_t readRet = NrfEeprom::readByte(nextInstructionAddy, &data);
        if (readRet == 0) {
            //sysmsg(LVL_EXTENDED, "EEPROM - Next instruction = %d", data);
            *nextInstructionCode = data;
            return EEPROM_SUCCESS;
        } else {
            //sysmsg(LVL_ERROR, "EEPROM - i2c Error %d - Failed to read next instruction (%d)", readRet, nextInstructionAddy);
            return EEPROM_FAILURE;
        }
    } else {
        //sysmsg(LVL_ERROR, "EEPROM - i2c Error (3*)- Failed to read next instruction address");
        return EEPROM_FAILURE;
    }
}