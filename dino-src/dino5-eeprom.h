#ifndef DINO5_EEPROM_H
#define DINO5_EEPROM_H

/*
    dino5-eeprom.h

    Class for easy access to the NRF connected EEPROM
*/

#define EEPROM_ADDRESS          0xA0
// Size of EEPROM chip
#define MEM_SIZE                256

enum EepromError_e {
    EEPROM_SUCCESS,
    EEPROM_FAILURE
};

class NrfEeprom {
    
    public:
    
    static EepromError_e resetAquaInstructionPointer();
    static EepromError_e getNextInstructionAddress(char* nextAddy);
    static EepromError_e getStartMode(char* mode);
    static EepromError_e getNextInstruction(char* nextInstructionCode);

    static size_t writeByte(size_t wordAddress, char data);
    static size_t readByte(size_t wordAddress, char* data);

};

#endif  /* DINO5_EEPROM_H */