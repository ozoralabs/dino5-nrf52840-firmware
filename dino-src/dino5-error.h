#ifndef DINO5_ERROR_H
#define DINO5_ERROR_H

/*
    dino5-error.h

    Error class for the dino5 NRF that gives more descriptive error codes than just
    success and fail
*/

typedef enum Dino5Error_e {
    SUCCESS,
    FAIL
} Dino5Error_t;

void CriticalErrorLoop(void);

#endif