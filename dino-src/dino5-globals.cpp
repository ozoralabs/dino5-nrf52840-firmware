#include "mbed.h"
#include "dino5.h"

DinoSerial ser(DINO5_NRF_PIN_TX, DINO5_NRF_PIN_RX);
I2C i2c(DINO5_NRF_PIN_I2C_SDA, DINO5_NRF_PIN_I2C_SCL);

DinoRtc rtc(rtc.defaulttime);

Sys sys(sys.sysdefaults);

Led led(0);

DinoWdog wdog(wdog.wdogdefault);

WatchdogTimer wdogTimer(3);

GnssComm gnss(gnss.gnssdefaults);

DigitalOut sds(p38), ehs6_en(p24), ehs6_on(p17);
DigitalOut redled(PIN_LED_RED), grnled(PIN_LED_GREEN), bluled(PIN_LED_BLUE);
DigitalOut usb_select0(p34), usb_select1(p35);
DigitalOut ensurepsw(p30);

DigitalOut eva_en(DINO5_NRF_GPS_ENABLE);

DigitalIn ehs6_flow_out(DINO5_NRF_EHS6_FLOW_OUT);
DigitalOut ehs6_flow_in(DINO5_NRF_EHS6_FLOW_IN);

SDBlockDevice sd(PIN_SD_CARD_MOSI, PIN_SD_CARD_MISO, PIN_SD_CARD_CLK, PIN_SD_CARD_SS);
FATFileSystem fs("sd", &sd); 