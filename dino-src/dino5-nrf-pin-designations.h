#ifndef DINO5_NRF_PIN_DESIGNATIONS_H
#define DINO5_NRF_PIN_DESIGNATIONS_H

/*
    dino5-nrf-pin-designations.h

    Abstractions for pin numbers on the NRF
*/

#define PIN_LED_RED                         p6
#define PIN_LED_GREEN                       p14
#define PIN_LED_BLUE                        p8

#define DINO5_NRF_PIN_TX                    p11
#define DINO5_NRF_PIN_RX                    p13

#define DINO5_NRF_GPS_ENABLE                p33

#define PIN_SD_CARD_MOSI                    p4
#define PIN_SD_CARD_MISO                    p5
#define PIN_SD_CARD_CLK                     p3
#define PIN_SD_CARD_SS                      p2

#define DINO5_NRF_PIN_I2C_SDA               p15
#define DINO5_NRF_PIN_I2C_SCL               p16

#define DINO5_NRF_EHS6_FLOW_OUT             p45
#define DINO5_NRF_EHS6_FLOW_IN              p44

#endif