#ifndef DINO5_H
#define DINO5_H

#include <string>
#include <vector>

/*
    dino5.h

    Connected include file for all the dino5 NRF source files.
*/

#include "dino5-nrf-pin-designations.h"
#include "BLE.h"
#include "WatchdogTimer.h"
#include "FATFileSystem.h"
#include "SDBlockDevice.h"
#include "MbedJSONValue.h"
#include "sd-card-tests.h"
#include "simple-i2c.h"
#include "dino5-eeprom.h"
#include "dino5-globals.h"
#include "BufferedSerial.h"
#include "dino-serial.h"
#include "dino5-error.h"
#include "dino-rtc.h"
#include "si7006.h"
#include "wdog.h"
#include "sys.h"
#include "led.h"
#include "dsen.h"
#include "gnsscomm.h"

#endif  /* DINO5_H */