#include "dsen.h"
#include "dino5.h"

extern DinoRtc rtc;

DSen::DSen()
{
    this->root["uuid"] = getRandomUuid();
}

DSen::~DSen() = default;

string DSen::getRandomUuid(void)
{
    unsigned int seg[13];
    char uuidbuf[30];
    int i;

    srand(time(NULL));

    (void) rand(); //First call to rand is 0, so lets discard it

    for (i=0;i<13;i++)
    {
        seg[i] = rand() % 0xEF + 0x10;
    }

    snprintf(uuidbuf, 30, "%X%X%X%X-%X%X-%X%X-%X%X%X%X%X\r\n", seg[0], seg[1], \
        seg[2], seg[3], seg[4], seg[5], seg[6], seg[7], seg[8], seg[9], seg[10], \
        seg[11], seg[12]);

    return string(uuidbuf);
}

string DSen::buildAndGetSenStr(void)
{
    string dt;
    rtc.getCdateFmt(dt, RTC_FMT_DSEN);

    this->datetime = dt;

    this->root["datetime"] = this->datetime;
    this->root["gpsminimum"] = getGpsMinimum();
    this->root["gpsextended"] = getGpsExtended();
    this->root["sensors"] = getSensorData();
    this->root["ble"] = getBle();
    this->root["custom"] = getCustom();

    this->root["incoming_ip"] = "68.15.55.192";
    this->root["install_id"] = "abcdefghijklmnop";

    return this->root.serialize();
}

MbedJSONValue DSen::getGpsMinimum(void)
{
    MbedJSONValue gpsminimum;

    gpsminimum["time"] = this->datetime;
    gpsminimum["numsat"] = "6";
    gpsminimum["lon"] = this->lon;
    gpsminimum["lat"] = this->lat;
    gpsminimum["height"] = "35.5686";
    gpsminimum["gspeed"] = "0.0543";
    gpsminimum["direction"] = "354.6796";

    return gpsminimum;
}

MbedJSONValue DSen::getGpsExtended(void)
{
    MbedJSONValue gpsextended; 

    gpsextended["DOP"] = "18.3411,-88.9030,41.1214,-133.7449";

    return gpsextended;
}

MbedJSONValue DSen::getSensorData(void)
{
    MbedJSONValue sensors;
    
    sensors["pct_battery"] = "86";
    sensors["accelerometer"] = "0,34,5";
    sensors["temperature"] = getFTempStr();
    sensors["humidity"] = "10";
    sensors["pressure"] = "5";
    sensors["update_rate"] = "1";

    return sensors;
}

MbedJSONValue DSen::getBle(void)
{
    MbedJSONValue ble;
    
    ble["devices_seen"][0] = "AA:BB:CC:DD:EE:77";
    ble["devices_seen"][1] = "11:BB:CC:DD:66:FF";
    ble["devices_seen"][2] = "AA:22:CC:DD:55:FF";
    ble["devices_seen"][3] = "AA:37:33:DD:EE:88";
    ble["devices_seen"][4] = "23:BB:CC:44:EE:FF";

    return ble;
}

MbedJSONValue DSen::getCustom(void)
{
    MbedJSONValue custom;

    custom["DOP"] = "18.3411,-88.9030,41.1214,-133.7449";

    return custom;
}

void DSen::setCoordinates(double lon, double lat)
{
    this->lon = lon;
    this->lat = lat;
}

