#ifndef DSEN_H
#define DSEN_H

#include "dino5.h"

/*
    dsen.h

    'Dino Sentence' builder class. A dino sentence is the only text string 
    (a JSON object) that is sent to the 3G modem (EHS6) during a data
    upload. This class provides methods to build and adjust this string
    before sending.
*/

class DSen
{
    private:
        MbedJSONValue root;

        double lon, lat;

        string datetime;

        string getRandomUuid(void);
        MbedJSONValue getGpsMinimum(void);
        MbedJSONValue getGpsExtended(void);
        MbedJSONValue getSensorData(void);
        MbedJSONValue getBle(void);
        MbedJSONValue getCustom(void);

    public:
        DSen();
        ~DSen();

        string buildAndGetSenStr(void);
        void setCoordinates(double lon, double lat);
};

#endif