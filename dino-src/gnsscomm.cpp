#include "gnsscomm.h"
#include "gnss-cmds.h"
#include "dino5.h"
#include "mbed.h"

extern DigitalOut eva_en;
extern DinoSerial ser;

GnssComm::GnssComm(NavMinimum_t defaults)
{
    this->mindata = defaults;
}

GnssComm::~GnssComm() = default;

void GnssComm::powerOnEva(void)
{
    eva_en = 1;
    wait(0.1);
}

void GnssComm::powerOffEva(void)
{
    eva_en = 0;
    wait(0.1);
}

void GnssComm::silenceAllInitialNmea(void)
{
    wait(1);
    for(int i=0;i<13;i++)
    {
        while(SUCCESS != ser.sendEvaCmdAndCheckReply \
            (ubx_expected_silencing_reply, 10, ubx_cmd_silence_nmea.at(i), 11, 100))
        {
            wait(2);
        }
    }
}

Dino5Error_t GnssComm::ubx_PollPvtForFix(void)
{
    char response_buffer[200];
    int expected_response_length = 100; //Payload is 92 bytes, +6 for header and +2 for checksum = 100
    int i;

    ser.sendEvaCmdAndGetReply(response_buffer, expected_response_length, ubx_cmd_poll_pvt, 8, 1000); 
    wait(.1);
    ser.selectPcSerial();
    ser.printf("Got response: \r\n");
    wait(.1);
    for(i=0;i<expected_response_length;i++) {
        ser.printf("%X ", response_buffer[i]);
    }
    ser.printf("\r\n\r\n");
    wait(.1);
    this->mindata.fixType = response_buffer[26];

    this->mindata.year[0] = response_buffer[10];
    this->mindata.year[1] = response_buffer[11];

    uint16_t yr = (this->mindata.year[1] << 8) | this->mindata.year[0];

    this->mindata.longitude[0] = response_buffer[30];
    this->mindata.longitude[1] = response_buffer[31];
    this->mindata.longitude[2] = response_buffer[32];
    this->mindata.longitude[3] = response_buffer[33];

    this->mindata.latitude[0] = response_buffer[34];
    this->mindata.latitude[1] = response_buffer[35];
    this->mindata.latitude[2] = response_buffer[36];
    this->mindata.latitude[3] = response_buffer[37];

    this->mindata.dlon = 0.0000001 * (signed long) ((this->mindata.longitude[3] << 24) | (this->mindata.longitude[2] << 16) | (this->mindata.longitude[1] << 8) | this->mindata.longitude[0]);
    this->mindata.dlat = 0.0000001 * (signed long) ((this->mindata.latitude[3] << 24) | (this->mindata.latitude[2] << 16) | (this->mindata.latitude[1] << 8) | this->mindata.latitude[0]);

    this->mindata.month = response_buffer[12];
    this->mindata.day = response_buffer[13];
    this->mindata.hour = response_buffer[14];
    this->mindata.minute = response_buffer[15];
    this->mindata.second = response_buffer[16];
    
    if ((this->mindata.fixType == 3) || (this->mindata.fixType == 2)) return SUCCESS;
    return FAIL;

}

double GnssComm::getDlon(void)
{
    return this->mindata.dlon;
}

double GnssComm::getDlat(void)
{
    return this->mindata.dlat;
}