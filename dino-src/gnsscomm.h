#ifndef GNSSCOMM_H
#define GNSSCOMM_H

#include "dino5.h"
#include "dino-serial.h"

/*
    gnsscomm.h

    Interface methods and data storage types for communication with the
    GNSS tranceiver (the on-board u-blox EVA-M8Q). NavMinimum_t contains
    the latest gnss coordinate update, if there was one.
*/

typedef struct NavMinimum_t {
    unsigned char year[2];
    unsigned char month;
    unsigned char day;
    unsigned char hour;
    unsigned char minute;
    unsigned char second;

    unsigned char valid;

    unsigned char fixType;

    unsigned char numSatellites;
    
    unsigned char longitude[4];
    unsigned char latitude[4];

    double dlon;
    double dlat;
    
    unsigned char gspeed[4];

} NavMinimum_t;

class GnssComm
{
    private:
        NavMinimum_t mindata;

    public:
        GnssComm(NavMinimum_t defaults);
        ~GnssComm();

        void powerOnEva(void);
        void powerOffEva(void);
        void silenceAllInitialNmea(void);
        double getDlon(void);
        double getDlat(void);

        Dino5Error_t ubx_PollPvtForFix(void);

        NavMinimum_t gnssdefaults = {0};
};

#endif