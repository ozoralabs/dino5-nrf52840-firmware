#include "led.h"
#include "dino5.h"

extern DigitalOut redled, grnled, bluled;

Led::Led(int init)
{
    (void) init;
}

Led::~Led()
{
    return;
}

void Led::blinkRedLed(float s)
{
    redled = 1;
    wait(s);
    redled = 0;
}

void Led::blinkGreenLed(float s)
{
    grnled = 1;
    wait(s);
    grnled = 0;
}

void Led::blinkBlueLed(float s)
{
    bluled = 1;
    wait(s);
    bluled = 0;
}