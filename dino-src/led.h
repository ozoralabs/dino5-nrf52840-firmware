#ifndef LED_H
#define LED_H

#include "dino5.h"

/*
    led.h

    Class that makes abstractions for controlling either the large eye LED,
    or the five small accent LEDs.
*/

class Led
{
    public:
        Led(int init);
        ~Led();

        void blinkRedLed(float s);
        void blinkGreenLed(float s);
        void blinkBlueLed(float s);

    };

#endif