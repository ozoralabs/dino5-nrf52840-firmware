#include "mbed.h"
#include "dino5.h"
#include "si7006.h"

// Si7006 Command Codes
const char hum_hold =       0xE5;
const char hum_no_hold =    0xF5;
const char temp_hold =      0xE3;
const char temp_no_hold =   0xF3;
const char temp_RH_hold =   0xE0;
const char reset =          0xFE;
const char reg_write =      0xE6;
const char reg_read =       0xE7;
const char heat_write =     0x51;
const char heat_read =      0x11;

//I2C Initialization
extern I2C i2c;

// Read temperature code (Hold Master Mode)
int readTemp(char *data){
    if(i2c.write(SLAVE_ADDR, &temp_hold, 1) != 0){
        return 1;
    }
    else { 
        if(i2c.read(SLAVE_ADDR, data, 2) != 0){
            return 2;
        }
        else 
            return 0;
    }
}

// Convert temp code to Celsius
float celsius(char *data){
    return (((data[0] * 256 + data[1]) * 175.72) / 65536.0) - 46.85;
}

// Convert temp code to Fahrenheit
float fahrenheit(char *data){
    return (celsius(data)) *1.8 + 32;
}            
        
// Read humidity code
int readHumidity(char *data){
    if(i2c.write(SLAVE_ADDR, &hum_hold, 1) != 0){
        return 1;
    }
    else {
        if(i2c.read(SLAVE_ADDR, data, 2) != 0){
            return 2;
        }
        else
            return 0;
    }
}

// Convert humidity code to relative humidity 
float humidity(char *data){
    return (((data[0] * 256 + data[1]) * 125.0) / 65536.0) - 6;
}
      
// Reset 
bool resetRegs(){
    if(i2c.write(SLAVE_ADDR, &reset, 1) != 0){
        return false;
    }
    else
        return true;
}

// Write User Register
bool writeReg(char data){
    char cmd[2];
    cmd[0] = reg_write;
    cmd[1] = data;
    if(i2c.write(SLAVE_ADDR, cmd, 2) != 0){
        return false;
    }
    else
        return true;
}

// Read User Register
int readReg(char *data){
    if(i2c.write(SLAVE_ADDR, &reg_read, 1) != 0){
        return 1;
    }
    else{ 
        if(i2c.read(SLAVE_ADDR, data, 1) != 0){
            return 2;
        }
        else
           return 0;
    }
}           

// Write Heater Control Register    
bool writeHeat(char data){
    char cmd[2];
    cmd[0] = heat_write;
    cmd[1] = data;
    if(i2c.write(SLAVE_ADDR, cmd, 2) != 0){
        return false;
    }
    else
        return true;
}

// Read Heater Control Register
int readHeat(char *data){
    if(i2c.write(SLAVE_ADDR, &heat_read, 1) != 0){
        return 1;
    }
    else{ 
        if(i2c.read(SLAVE_ADDR, data, 1) != 0){
            return 2;
        }
        else
           return 0;
    }
}

string getFTempStr(void)
{
    string ret("0.00F");

    char data[2];
    int res = readTemp(data);
    if(res == 0){
        float fTemp = fahrenheit(data);
        char buf[8];
        snprintf(buf, 8, "%.2fF", fTemp);
        ret = string(buf);
    }
    return ret;
}