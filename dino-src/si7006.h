#ifndef DINO5_Si7006_H
#define DINO5_Si7006_H

#define SLAVE_ADDR  (0x80)

extern I2C i2c;

int readTemp(char *data);
float celsius(char *data);
float fahrenheit(char *data);
int readHumidity(char *data);
float humidity(char *data);
bool resetRegs();
bool writeReg(char data);
int readReg(char *data);

string getFTempStr(void);

#endif