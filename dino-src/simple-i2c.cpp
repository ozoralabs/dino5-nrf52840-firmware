#include "mbed.h"
#include "dino5.h"
#include "simple-i2c.h"

extern I2C i2c;

bool writeI2cByte(uint8_t slave_addr, uint8_t mem_addr, uint8_t data)
{
	char cmd[2];
	cmd[0] = mem_addr;
	cmd[1] = data;
	int ret = i2c.write(slave_addr, cmd, 2, false);

	if (ret == 0) return true;
	return false;

}

bool writeI2cStr(uint8_t slave_addr, uint8_t mem_addr, uint8_t* data, uint8_t len)
{
	char cmd[len + 1];
	cmd[0] = mem_addr;

  int i;

  for (i=0;i<len;i++) {
    cmd[i+1] = data[i];
  }

	int ret = i2c.write(slave_addr, cmd, len + 1, false);

	if (ret == 0) return true;
	return false;

}

bool readI2cByte(uint8_t slave_addr, uint8_t mem_addr, char* data)
{
	char cmd[1];
	cmd[0] = mem_addr;
	int ret = i2c.write(slave_addr, cmd, 1, false);
	wait(0.1);
	if (ret == 0) {
		ret = i2c.read(slave_addr, data, 1, false);
		if (ret == 0) return true;
	}
	return false;
}

bool readI2cStr(uint8_t slave_addr, uint8_t mem_addr, char* data, int size)
{
	char cmd[1];
	cmd[0] = mem_addr;
	int ret = i2c.write(slave_addr, cmd, 1, false);
	wait(0.1);
	if (ret == 0) {
		ret = i2c.read(slave_addr, data, size, false);
		if (ret == 0) return true;
	}
	return false;
}