#ifndef SIMPLE_I2C_H
#define SIMPLE_I2C_H

/*
    simple-i2c.h

    Small abstraction library to make simple i2c reads and writes easy.
*/

bool writeI2cByte(uint8_t slave_addr, uint8_t mem_addr, uint8_t data);
bool writeI2cStr(uint8_t slave_addr, uint8_t mem_addr, uint8_t* data, uint8_t len);
bool readI2cByte(uint8_t slave_addr, uint8_t mem_addr, char* data);
bool readI2cStr(uint8_t slave_addr, uint8_t mem_addr, char* data, int size);

#endif  /* SIMPLE_I2C_H */