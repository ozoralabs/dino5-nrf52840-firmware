#include "sys.h"
#include "dino5.h"

extern DinoSerial ser;
extern DinoRtc rtc;
extern SDBlockDevice sd;
extern FATFileSystem fs;
extern DigitalOut sds, ensurepsw;
extern DinoWdog wdog;
extern Led led;

Sys::Sys(SysDefaults_t d)
{
    this->SerialMode = d.SerialMode;
    this->SdLogMode = d.SdLogMode;
}

Sys::~Sys()
{
    return;
}

void Sys::startup(void)
{
    this->guaranteeNrfPower();
    wdog.start();
    led.blinkGreenLed(.1);
}

void Sys::guaranteeNrfPower(void)
{
    ensurepsw = 1;
}

void Sys::releaseNrfPowerGuarantee(void)
{
    ensurepsw = 0;
}

void Sys::sendSerialBasedOnMode(string msg)
{
    switch (this->SerialMode)
    {
        case SYS_SERIAL_VIA_DAPLINK:
            ser.selectPcSerial();
            ser.printf(msg.c_str());
            return;
        case SYS_SERIAL_VIA_DBPORT:
            ser.selectDebugSerial();
            ser.printf(msg.c_str());
            return;
        case SYS_SERIAL_VIA_BOTH:
            ser.selectPcSerial();
            ser.printf(msg.c_str());
            ser.selectDebugSerial();
            ser.printf(msg.c_str());
            return;
        case SYS_SERIAL_VIA_NONE:
            return;
    }
}

Dino5Error_t Sys::ez_sdwrite(string msg)
{
    sds = 1;
			
    FILE *fp = fopen("/sd/log.txt", "ab+");
    if(fp == NULL) {
        return FAIL;
    }
    fprintf(fp, msg.c_str());
    fclose(fp);

    sds = 0;

    return SUCCESS;
}

void Sys::writeSdBasedOnLvl(SysLvl_t lvl, string msg)
{
    switch(this->SdLogMode)
    {
        case SYS_SD_LOG_ALL:
            ez_sdwrite(msg);
            return;
        case SYS_SD_LOG_WARN_AND_ERRORS:
            if((lvl == WARN) || (lvl == ERROR))
            {
                ez_sdwrite(msg);
            }
            return;
        case SYS_SD_LOG_ERRORS_ONLY:
            if(lvl == ERROR)
            {
                ez_sdwrite(msg);
            }
            return;
        case SYS_SD_LOG_OFF:
            return;
    }
}

void Sys::msg(SysLvl_t lvl, string msg)
{
    string cdate_fmt, fullmsg;
    
    rtc.getCdateFmt(cdate_fmt, RTC_FMT_LOG);

    fullmsg = cdate_fmt + " " + this->strlvls[lvl] + ": " + msg + "\r\n";

    sendSerialBasedOnMode(fullmsg);
    writeSdBasedOnLvl(lvl, fullmsg);
}