#ifndef SYS_H
#define SYS_H

#include "dino5.h"

/*
    sys.h

    General system class dealing with methods from gpio control, wrapped
    debug outputs, and low level error checking. 
*/

typedef enum SysSerialMode_e
{
    SYS_SERIAL_VIA_DAPLINK,
    SYS_SERIAL_VIA_DBPORT,
    SYS_SERIAL_VIA_BOTH,
    SYS_SERIAL_VIA_NONE
} SysSerialMode_t;

typedef enum SysSdLogMode_e
{
    SYS_SD_LOG_ALL,
    SYS_SD_LOG_WARN_AND_ERRORS,
    SYS_SD_LOG_ERRORS_ONLY,
    SYS_SD_LOG_OFF
} SysSdLogMode_t;

typedef struct SysDefaults_t
{
    SysSerialMode_t SerialMode;
    SysSdLogMode_t SdLogMode;
} SysDefaults_t;

typedef enum SysLvl_e
{
    INFO = 0,
    WARN,
    ERROR
} SysLvl_t;

class Sys
{
    private:
        SysSerialMode_t SerialMode;
        SysSdLogMode_t SdLogMode;
        Ticker errticker;

        void sendSerialBasedOnMode(string msg);
        Dino5Error_t ez_sdwrite(string msg);
        void writeSdBasedOnLvl(SysLvl_t lvl, string msg);

        string strlvls[3] = {"INFO", "WARN", "ERRO"};

    public:
        Sys(SysDefaults_t d);
        ~Sys();

        void msg(SysLvl_t lvl, string msg);

        void startup(void);

        void guaranteeNrfPower(void);
        void releaseNrfPowerGuarantee(void);

        SysDefaults_t sysdefaults = {SYS_SERIAL_VIA_DAPLINK, SYS_SD_LOG_WARN_AND_ERRORS};
};

#endif