#include "mbed.h"
#include "wdog.h"

extern WatchdogTimer wdogTimer;

DinoWdog::DinoWdog(int wdogTimeoutTime)
{
    this->wdogdefault = wdogTimeoutTime;
}

DinoWdog::~DinoWdog()
{
    return;
}

static void wdogCallback(void)
{
    wdogTimer.kick();
} 

void DinoWdog::start(void)
{
    this->wdogTicker.attach(wdogCallback, 1);
}