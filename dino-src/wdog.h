#ifndef WDOG_H
#define WDOG_H

#include "mbed.h"
#include "WatchdogTimer.h"

/*
    wdog.h

    Basic watchdog class that functions to reset the device if it encounters 
    a system lockup
*/

class DinoWdog
{
    private:
        Ticker wdogTicker;
        
    public:
        DinoWdog(int wdogTimeoutTime);
        ~DinoWdog();

        int wdogdefault = 3;
        void start(void);
};

#endif