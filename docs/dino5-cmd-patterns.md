## CommandPattern

This parameter controls the function and workflow of how the dino5 operates. It is composed of a series of commands that control the 3 main modules in the dino5: Navigation, Bluetooth and 3G connectivity. There are 15 total commands recognized by the dino5. 6 of these are 'one-shot' commands -- meaning they are executed by the dino5 for one objective, and when that objective is completed, the dino5 continues to the next command. The remaining 9 commands are 'interval' commands -- meaning they are executed by the dino5 for a certain period of time, and once this time is up, the dino5 moves on to the next command. The command list is here:

## One-Shot commands

NAV 
- Run the ‘NAV_command()’ function in the NRF52 firmware (see Section 9d). By default, this will attain a navigation fix execute any logging that is enabled (See  ‘GPXLogging’ – Section 7g)

3G	
- Run the ‘3G_command()’ function in the NRF52 firmware (see Section 9d). By default, this will attain a 3G connection and send a standard AQSEN packet to the server with all currently available information.

3GNAV 
-	Run the ‘3GNAV_command()’ function in the NRF52 firmware (see Section 9d). By default, this will attain a navigation fix and 3G connection, and then send a standard AQSEN packet to the server with all currently available information.

3GBLE 
-	Run the ‘3GBLE_command()’ function in the NRF52 firmware (see Section 9d). By default, this will absorb Bluetooth while attaining a 3G connection, and then send a standard AQSEN packet to the server with all currently available information.

NAVBLE 
- Run the ‘NAVBLE _command()’ function in the NRF52 firmware (see Section 9d). By default, this will absorb and log Bluetooth while attaining a navigation fix. Then any logging that is enabled will be executed (See ‘GPXLogging’ – Section 7g).

3GNAVBLE 
- Run the ‘3GNAVBLE_command()’ function in the NRF52 firmware (see Section 9d). By default, this will absorb and log Bluetooth while attaining a navigation fix and 3G connection. Then the dino5 will send a standard AQSEN packet to the server with all currently available information.

## Interval commands

DS(n)
- Requirements: 0 < n < 65535	
- Deep sleep for n minutes (nRF52 off – accelerometer data will be ignored)

S(n)
- Requirements: 0 < n < 65535	
- Sleep for n minutes (nRF52 on in a reduced power state – accelerometer data will be gathered).

NAV(x:n)
- Requirements: 0 < x < n < 65535	
- Run the ‘NAVxn_command()’ function in the NRF52 firmware (see Section 9d). By default, this will attain a navigation fix and keep it open for n minutes. Every x minutes, the enabled logging will be executed.

3G(x:n)
- Requirements: 0 < x < n < 65535
- Run the ‘3Gxn_command()’ function in the NRF52 firmware (see Section 9d). By default, this will attain a 3G connection and keep it open for n minutes. Every x minutes, a standard AQSEN packet will be sent to the server with all currently available information.

BLE(x:n)
- Requirements: 0 < x < n < 65535
- Run the ‘BLExn_command()’ function in the NRF52 firmware (see Section 9d). By default, this will absorb Bluetooth for n minutes. Every x minutes, all absorbed advertisements and the respective mac addresses in the buffer will be written to the enabled logs.

3GNAV(x:n)
- Requirements: 0 < x < n < 65535
- Run the ‘3GNAVxn_command()’ function in the NRF52 firmware (see Section 9d). By default, this will attain a navigation fix and a 3G connection and keep them open for n minutes. Every x minutes, a standard AQSEN packet will be sent to the server with all currently available information.

3GBLE(x:n)
- Requirements: 0 < x < n < 65535
- Run the ‘3GBLExn_command()’ function in the NRF52 firmware (see Section 9d). By default, this will absorb Bluetooth, as well as attain a 3G connection and keep it open, for n minutes. Every x minutes, a standard AQSEN packet will be sent to the server with all currently available information.

NAVBLE(x:n)
- Requirements: 0 < x < n < 65535
- Run the ‘NAVBLExn_command()’ function in the NRF52 firmware (see Section 9d). By default, this will absorb Bluetooth, as well as attain a navigation fix and keep it open, for n minutes. Every x minutes, the enabled logging will be executed.

3GNAVBLE(x:n)
- Requirements: 0 < x < n < 65535
- Run the ‘3GNAVBLExn_command()’ function in the NRF52 firmware (see Section 9d). By default, this will absorb Bluetooth, attain a navigation fix and keep it open, and attain a 3G connection and keep it open, for n minutes. Every x minutes, a standard AQSEN packet will be sent to the server with all currently available information.

There are also 2 modes in which the CommandPattern parameter is executed: ‘Continuous Interval Mode’ and ‘GMT Start Mode’. 
-	Continuous Interval Mode will execute the CommandPattern commands immediately upon getting power and then loop around to the first command after finishing the last. This is indicated by a ‘C’ at the beginning of the CommandPattern value.
-	GMT Start Mode will wait until a specific time of day to execute the first command. When the final command in the CommandPattern string is completed, the dino5 will automatically run a Deep Sleep command until that specific time of day is reached again. This is indicated by a ‘Gh:m’ at the beginning of the CommandPattern value – where h:m is the time, in 24 hour format, to start the CommandPattern commands.


## CommandPattern Format
CommandPattern =	Effect
<C,G(h1:m1-h2:m2)>;<Cmdx>([A]),<Cmdx>([A]), … <Cmdx>([A]);

Where <C,G(h1:m1-h2:m2)> is either a ‘C’ for Continuous Interval Mode, or ‘G(h1:m2-h2:m2)’ for GMT Start Mode where h1:m1 is the GMT time to start (in 24 hour format) the first command and h2:m2 is the time to stop looping through the command list. After stopping while in GMT Start Mode, the dino5 will go into deep sleep until h1:m1. The optional “[A]” after each command indicates whether the command should be considered as an ‘accelerometer-dependent’ command. 

<Cmdx> is any of the 17 commands from the One-Shot or Interval command tables.

## Accelerometer Dependent Commands
The optional ‘[A]’ after each command signifies whether a command should be considered as ‘Accelerometer Dependent’ by the dino5. Its purpose is mainly power-saving, and is relevant if you don’t mind ignoring data from the dino5 that is in the same spot as the previous command. If a command is Accelerometer Dependent, it will not execute an event (a one-shot command or intermediary execution for an interval command) unless the dino5 has moved since the last event. If, at the beginning of an event, the accelerometer concludes that the dino5 has remained stationary since the previous event, the dino5 will enter Sleep mode (But not Deep Sleep) instead of executing the event. Therefore, when the dino5 moves again, a new navigation fix will need to be attained before completing further ‘NAV’ type commands.

NOTE: The command DS(n) does not gather accelerometer data. Therefore, Accelerometer Dependent commands immediately after a DS(n) command will never execute, as the accelerometer couldn’t ascertain whether the dino5 has moved or not.

## CommandPattern Examples

C;3GNAV,DS(5);	This is the default CommandPattern loaded into config.txt. When the dino5 is turned on with this CommandPattern, it will look for a position fix, upload an AQSEN object to the server, sleep for 5 minutes and repeat.

C;NAVBLE(5:60);	When powered on, the dino5 will absorb Bluetooth and attain and keep open a navigation fix. After 60 minutes, the command will be refreshed. The data will be logged every 5 minutes.

G(08:30-20:00);3GNAV(1:60)[A];	When powered on, deep sleep until 08:30 GMT. Then, open a continuous 3G connection and navigation fix. Send a standard AQSEN object to the TargetServer every 1 minute – but only if the accelerometer concludes that we have moved since the last upload. Refresh this command every 60 minutes. If the time is after 20:00 GMT when we restart this loop, go into deep sleep until 08:30 and start again.

C;3VNAVBLE;BLE(5:15);	dino5 will absorb bluetooth advertisements while attaining a 3G connection and navigation fix when turned on. It will send an AQSEN object to TargetServer when that’s all complete. Then, the dino5 will absorb Bluetooth only, for 15 minutes, and write the absorption buffers to the enabled logs every 5 minutes.

G(06:00-00:00);3GNAVBLE[A],BLE(3:30);	When powered on, the dino5 will wait until 06:00 GMT. Then it will absorb bluetooth advertisements while attaining a 3G connection and navigation fix (Since this is the first execution, ‘[A]’ will be ignored for this time only). It will send an AQSEN object to TargetServer when that is complete. Then, the dino5 will absorb Bluetooth only, for 30 minutes, and write the absorption buffers to the enabled logs every 3 minutes. If the device moves during this time, then the ‘3GNAVBLE’ command will be executed again, but ignored if not. This loop of accelerometer dependent navigation fix uploads and Bluetooth absorption will continue until 24:00 GMT, at which time it will go into deep sleep until 06:00, and start the process over.
