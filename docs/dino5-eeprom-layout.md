The nRF52840 onboard the dino5, dino5x and dino5z communicate with a 2Kbit EEPROM chip (Microchip AT24C02D-MAHM-T) via I2C. It is the only non-volatile memory available to the nRF52840. The following document describes the protocol by which the nRF52840 utilizes this memory.

For information regarding the EEPROM's participation in system flow as it pertains to overall dino5 functionality, please refer to 'docs/dino5_System_Flowchart.pdf'

## dino5 EEPROM Memory Layout

_0x00-0x03 = deep sleep info for AM1805_
0x00 - Number of TOTAL deep sleep intervals OF ANY KIND remaining.
0x01 - 'Standard' deep sleep interval (always 256 min)
0x02 - Length of final deep sleep interval.

0x0A - Instruction being carried out currently (For the nrf52 to determine actions at powerup.)

0x0F - Is this NOT the very first power-up?

_SD card parameters_
0x10: DebugMode (1 = Off, 2 = Regular, 3 = Extended)

0x30-0x3F -> Errors. Date followed by error code.

_-- SLEEP PATTERN BYTES --_
0x40 - Start mode: 1 = Continuous interval mode, 2 = GMT start mode
0x41 - GMT start time hours (Only relevant if in GMT start mode): 0xdddhhhhh where 'h's are the hours in binary
0x42 - GMT start time minutes (Only relevant if in GMT start mode): 0xddmmmmmm where 'm's are the mins in binary
0x43 - GMT start time mode (Only relevant if in GMT start mode): if 'd', runs pattern every day at this time. if 'w', runs pattern on any weekday at this time. If 'e', runs pattern only on saturday and sunday at this time.
0x44&0x45 - GMT start mode pattern repeat (Only relevant in GMT start mode): 0-65535 representing number of times to repeat the indicated pattern.
0x46 - GMT start mode day of month (only valid if start time mode is 'm')

Pointer to next instruction/interval:

0x4F - Holds value from 0x50 to 0x6F that indicates which instruction/interval is to be executed upon next 'TC-Low wakeup'.

Encode the actual pattern

0x50 - First instruction
0x51&0x52 - First sleep interval
0x53 - Second instruction
0x54&0x55 - Second sleep interval
0x56 - Third instruction
0x57&0x58 - Third sleep interval

...

0x6F


RULES:

Pattern (0x50) must start with an instruction, followed by an interval, followed by an instruction, followed by an interval, etc, until 0x00 is read.