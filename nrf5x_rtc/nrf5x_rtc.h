#ifndef NRF5X_RTC_H
#define NRF5X_RTC_H

#include "mbed.h"

static class nrf5x_rtc {
    // class to create equivalent of time() and set_time()
    //   ...depends upon RTC1 to be running and to never stop -- a byproduct of instantiation of mbed library's "ticker"
    public:
    nrf5x_rtc();
    time_t time();
    int set_time(time_t rawtime);
    
    // these should be private
    private:
    time_t time_base;
    unsigned int rtc_previous;
    unsigned int ticks_per_second, counter_size_in_seconds;
    void update_rtc(); // similar to "time" but doesn't return the value, just updates underlying variables

} rtc;
#endif