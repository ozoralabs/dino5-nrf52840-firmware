#include "mbed.h"
#include "dino5.h"
#include "si7006.h"
#include "Si7006-tests.h"

extern DinoSerial ser;

// Read temperature and output over UART every 2 seconds
void getTempData(){
    char data[2];
    int res = readTemp(data);
    if(res == 0){
        float cTemp = celsius(data);
        float fTemp = fahrenheit(data);
        ser.ezprintf("Temperature in Celsius : %.2f C \n\r", cTemp);
        ser.ezprintf("Temperature in Fahrenheit : %.2f F \n\n\r", fTemp);
    }
    else if(res == 1){
        ser.ezprintf("Temperature Write Error \n\n\r");
    }
    else if(res == 2){
        ser.ezprintf("Temperature Read Error \n\n\r");
    }
    wait(2);
}

// Read humidity and output over UART every 2 seconds
void getHumidityData(){
    char data[2];
    int res = readHumidity(data);
    if(res == 0){
        float hum = humidity(data);
        ser.ezprintf("Relative Humidity : %.2f RH \n\n\r", hum);
    }
    else if(res == 1){
        ser.ezprintf("Humidity Write Error \n\n\r");
    }
    else if( res == 2){
        ser.ezprintf("Humidity Read Error \n\n\r");
    }
    wait(2);
}

// Read config byte from user register and output over UART
void readRegTest(){
    char data[1];
    int res = readReg(data);
    if(res == 0){
        ser.ezprintf("Byte read : 0x%X \n\n\r", data[0]);
    }
    else if(res == 1){
        ser.ezprintf("Command write error \n\n\r");
    }
    else if(res == 2){
        ser.ezprintf("User register read error \n\n\r");
    }
}    

// Write config byte to user register and read it back
void write_read_Reg(){
    char userReg = 0xBB; //0xBB -> 11bit res,Vdd ok, heater disable]
    if(writeReg(userReg)){
        ser.ezprintf("Byte written : 0x%X \n\n\r", userReg);
        readRegTest();
    }
    else
        ser.ezprintf("User Register Write Error \n\n\r");
}

// Write/read config byte before and after device reset
void resetTest(){
    write_read_Reg();
    if(resetRegs()){
        ser.ezprintf("Reset command successful \n\n\r");
        readRegTest();
    }
    else
        ser.ezprintf("Reset Error");
}

// Write config byte to heater register and read it back
void write_read_Heat(){
    char userReg = 0x01; //0x01 -> 11bit res,Vdd ok, heater disable]
    if(writeReg(userReg)){
        ser.ezprintf("Byte written : 0x%X \n\n\r", userReg);
        readRegTest();
    }
    else
        ser.ezprintf("User Register Write Error \n\n\r");
}  
