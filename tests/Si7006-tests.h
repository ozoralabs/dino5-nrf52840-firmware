#ifndef SI7006_TESTS_H
#define SI7006_TESTS_H

void getTempData();
void getHumidityData();
void readRegTest();
void write_read_Reg();
void resetTest();

#endif