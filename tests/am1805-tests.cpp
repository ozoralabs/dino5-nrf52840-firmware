#include "am1805-tests.h"
#include "dino5.h"
#include "mbed.h"

extern I2C i2c;
extern DinoSerial ser;

static Dino5Error_t debugWriteI2cByte(char slave_addr, char slave_reg, char data)
{
    if (writeI2cByte(slave_addr, slave_reg, data)) 
    {
        ser.printf("Write success.\r\n");
    } 
    else 
    {
        ser.printf("Write fail.\r\n");
        return FAIL;
    }
    return SUCCESS;
}

static Dino5Error_t debugReadSlaveI2cReg(char slave_addr, char slave_reg)
{
    char byteread = 0;

    if (readI2cByte(slave_addr, slave_reg, &byteread)) 
    {
        ser.printf("Read success.\r\n");
        ser.printf("Read Character: %X\r\n", byteread);
    }
    else 
    {
        ser.printf("Read fail.\r\n");
        return FAIL;
    }
    return SUCCESS;
}

void am1805_write_defaults(void)
{
    debugWriteI2cByte(0xD2, 0x11, 0x18);
    debugWriteI2cByte(0xD2, 0x30, 0x2D);
    debugWriteI2cByte(0xD2, 0x10, 0x05);
    debugWriteI2cByte(0xD2, 0x1C, 0x00);
}

void am1805_allow_calender_writes(void)
{
    debugWriteI2cByte(0xD2, 0x10, 0x05);
}

void am1805_prevent_calender_writes(void)
{
    debugWriteI2cByte(0xD2, 0x10, 0x04);
}

void am1805_read_defaults(void)
{
    debugReadSlaveI2cReg(0xD2, 0x11);
    debugReadSlaveI2cReg(0xD2, 0x30);
    debugReadSlaveI2cReg(0xD2, 0x10);
    debugReadSlaveI2cReg(0xD2, 0x1C);
}