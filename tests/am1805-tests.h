#ifndef AM1805_TESTS_H
#define AM1805_TESTS_H

void am1805_write_defaults(void);
void am1805_allow_calender_writes(void);
void am1805_prevent_calender_writes(void);
void am1805_read_defaults(void);

#endif