#include "mbed.h"
#include "dino5.h"
#include "ble-tests.h"

extern DinoSerial ser;

static void advertisementCallback(const Gap::AdvertisementCallbackParams_t *params)
{   
    ser.printf("%s \r\n Adv peerAddr: [%02x %02x %02x %02x %02x %02x] \r\n rssi: %d \r\n ScanResp: %u \r\n AdvType: %u\r\n\n", "[unk. date]",
           params->peerAddr[5], params->peerAddr[4], params->peerAddr[3], params->peerAddr[2], params->peerAddr[1], params->peerAddr[0],
           params->rssi, params->isScanResponse, params->type);
}

static void onBleInitError(BLE &ble, ble_error_t error)
{
    ser.printf("BLE Initalization error.\r\n");
}

static void bleInitComplete(BLE::InitializationCompleteCallbackContext *params)
{
    BLE& ble = params->ble;
    ble_error_t error = params->error;

    if (error != BLE_ERROR_NONE) {
        // In case of error, forward the error handling to onBleInitError
        onBleInitError(ble, error);
        return;
    }

    // Ensure that it is the default instance of BLE
    if(ble.getInstanceID() != BLE::DEFAULT_INSTANCE) {
        return;
    }
 
    ble.gap().setScanParams(500 /* scan interval */, 200 /* scan window */);
    ble.gap().startScan(advertisementCallback);
}

/**
 * bleBlocking_ObserverTest(): Listen for and post Bluetooth traffic over UART. Blocking function.
 * returns: void
 */

void bleBlocking_ObserverTest(void)
{
    BLE &ble = BLE::Instance();
    ble.init(bleInitComplete);

    for(;;) {
        ble.waitForEvent();
    }
}