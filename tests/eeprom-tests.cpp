#include "mbed.h"
#include "dino5.h"
#include "eeprom-tests.h"

//extern DinoSerial ser;

/**
 * eeprom_ReadAndWriteTest(): Writes and reads to every EEPROM register. Note:
 *      this function will erase the contents of the EEPROM.
 * returns: void
 */

void eeprom_ReadAndWriteTest(void)
{
    int i;
    char readData[256];

    //ser.printf("Writing 255-i to each EEPROM register...\r\n");

    // Write 255-i to each register in the EEPROM
    for(i=0;i<255;i++) {
        NrfEeprom::writeByte(i, 255 - i);
    }

    //ser.printf("Reading each value from EEPROM...\r\n");

    // Read each character of the EEPROM into readData
    for(i=0;i<255;i++) {
        NrfEeprom::readByte(i, &(readData[i]));
    }

    //ser.printf("Printing EEPROM values...\r\n");

    for(i=0;i<255;i++) {
        //ser.printf("%XX ", readData[i]);
    }

    //ser.printf("\r\nErasing EEPROM...\r\n");

    // Write 0 to each register in the EEPROM
    for(i=0;i<255;i++) {
        NrfEeprom::writeByte(i, 0);
    }

    //ser.printf("\r\nTest Complete.\r\n");

}