#include "ehs6-tests.h"
#include "dino5.h"
#include "tests.h"

extern DigitalOut ehs6_en, ehs6_on, ehs6_flow_in;
extern DigitalIn ehs6_flow_out;
extern DigitalOut redled, bluled;
extern DinoSerial ser;

void power_on_ehs6(void) {
    ehs6_en = 1;
    wait(1);
    ehs6_on = 1;
    wait(1);
}

void power_off_ehs6(void) {
    ehs6_en = 0;
    wait(1);
    ehs6_on = 0;
    wait(1);
}

void ez_ehs6strsend(const char* str, int length)
{
    wait(1);
    ser.selectEhs6Serial();
    wait(1);
    for(int i=0;i<length;i++)
    {
        ser.putc(str[i]);
        wait(.005); //Must be kept at 5ms or higher, or else the nrf can halt
    }
    wait(.005);
    ser.putc('!');
    wait(.1);
}

void pass_string_to_ehs6(const char* str, int length) {

    bool sendsuccess = false;
    

    while(sendsuccess == false)
    {
        int idlectr = 0;

        bluled = 1;
        redled = 1;
        wait(.1);
        bluled = 0;
        redled = 0;
        wait(.1);
        bluled = 1;
        redled = 1;
        wait(.1);
        bluled = 0;
        redled = 0;

        power_on_ehs6();
        wait(8);

        ser.selectPcSerial();

        ehs6_flow_in = 1; //Tell EHS6 we are making a request
        while (ehs6_flow_out == 1) { //Wait until EHS6 triggers the 'ready for data' GPIO
            ser.selectPcSerial();
            wait(.1);
            ser.printf("flow out is not low yet.\r\n");
            wait(1);
        }
        ser.printf("flow out is low! \r\n");
        wait(3);
        ez_ehs6strsend(str, length);

        ser.selectPcSerial();
        wait(.1);
        ser.printf("waiting... \r\n");
        while(idlectr < 40) // Wait until EHS6 confirms data transmission
        {
            ser.selectPcSerial();
            wait(1);
            ser.printf("flow out is not low yet!!\r\n");
            wait(.1);
            if(ehs6_flow_out == 1)
            {
                sendsuccess = true;
                break;
            }
            idlectr++;
        }

        power_off_ehs6();
        wait(8);
    }

    ser.selectPcSerial();
    wait(1);
    ser.printf("Data sent and received!\r\n");

    ehs6_flow_in = 0;

}

Dino5Error_t simpleCoordinateUpdateToServer(void) {
    /*power_on_eva();
    wait(1);
    ubx_SilenceAll();
    wait(1);

    NavPvt_t NavPvt_o;

    while (ubx_PollPvtForFix(&NavPvt_o) != SUCCESS) {
        ser.selectPcSerial();
        wait(1);
        ser.printf("No 2d fix yet... retrying\r\n");
        wait(1);
    }

    power_on_ehs6();

    char cbuf[50];
    sprintf(cbuf, "\"lon\":\"%0.7f\",\"lat\":\"%0.7f\"", NavPvt_o.dlon, NavPvt_o.dlat);
    ser.selectPcSerial();

    wait(4); //wait for ehs6 to boot

    ser.printf("passing str: [ %s ]\r\n", cbuf);
    wait(1);
    pass_string_to_ehs6(cbuf);

    power_off_ehs6();
    power_off_eva();*/

    return SUCCESS;
}