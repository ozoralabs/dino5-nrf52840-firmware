#ifndef EHS6_TESTS_H
#define EHS6_TESTS_H

#include "mbed.h"
#include "dino5.h"

void power_on_ehs6(void);
void power_off_ehs6(void);
void ez_ehs6strsend(const char* str, int length);
void pass_string_to_ehs6(const char* str, int length);
Dino5Error_t simpleCoordinateUpdateToServer(void);

#endif  /* EHS6_TESTS_H */