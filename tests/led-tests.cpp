#include "mbed.h"
#include "dino5.h"
#include "led-tests.h"

Ticker ticker;
int toggleVar = 0;

//extern DinoSerial ser; // Globals defined in dino-src/dino5-globals.cpp
extern DigitalOut redled;
extern DigitalOut grnled;
extern DigitalOut bluled;

/**
 * tickerAsyncCallback(): Progesses thru a toggle pattern for the
 * nrf52840-dk leds each time it is called.
 * returns:             void
 * */

static void tickerAsyncCallback(void)
{
    toggleVar++;
    switch (toggleVar) {
    case 1:
        redled = 1; grnled = 0; bluled = 0;
        break;
    case 2:
        redled = 0; grnled = 1; bluled = 0;
        break;
    case 3:
        redled = 0; grnled = 0; bluled = 1;
        toggleVar = 0;
        break;
    default:
        toggleVar = 0;
    } 
}

/**
 * asyncLed_BlinkTest(): Schedule the 'tickerAsyncCallback' funtion
 * to run every 500ms
 * returns:             void
 */

void asyncLed_BlinkTest(void)
{
    // Attach async function to the ticker that runs every 500ms
    //ser.printf("Scheduling LED toggler...\r\n");
    ticker.attach(tickerAsyncCallback, 0.5);
}