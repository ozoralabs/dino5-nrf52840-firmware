#include "mbed.h"
#include "dino5.h"
#include "lz-tests.h"
#include "lz.h"

extern DinoSerial ser;

void lz_compressionTest(void)
{
    ser.printf("Starting LZ Compression test...\r\n");

    unsigned char strtest[50] = "!Compress me!", compStored[50], uncomped[50];

    memset(compStored, 0, 50);
    memset(uncomped, 0, 50);

    ser.printf("Compressing this string: %s\r\n", strtest);

    ser.printf("Compressing...\r\n");

    LZ_Compress(strtest, compStored, 14);

    ser.printf("Compressed. Blob looks like this: %X\r\n", compStored);

    ser.printf("Uncompressing...\r\n");

    LZ_Uncompress(compStored, uncomped, 14);

    ser.printf("Uncompressed string looks like: %s\r\n", uncomped);

    ser.printf("Test complete.\r\n");

}