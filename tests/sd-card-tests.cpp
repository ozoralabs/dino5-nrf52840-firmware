#include "mbed.h"
#include "dino5.h"
#include "sd-card-tests.h"

//extern DinoSerial ser; // Globals defined in dino-src/dino5-globals.cpp
extern SDBlockDevice sd;
extern FATFileSystem fs;
extern DigitalOut sds, grnled;
/**
 * sdCardTest_WriteTest(): Basic write test for SD card
 * returns:         true if completed successfully,
 *                  false if test fails
 */

void sdCardTest_WriteTest(void) 
{
    sds = 1;
    grnled = 1;
    wait(0.1);
    grnled = 0;
    wait(0.1);
			
    FILE *fp = fopen("/sd/sdtest.txt", "w");
    if(fp == NULL) {
        for(;;) {
            grnled = 1;
            wait(1);
            grnled = 0;
            wait(1);
        }
    }
    fprintf(fp, "Hello fun SD Card World!");
    fclose(fp);

    for(;;) {
        grnled = 1;
        wait(0.1);
        grnled = 0;
        wait(0.1);
    }
}