#include "mbed.h"
#include "dino5.h"
#include "tests.h"
#include "system-tests.h"

extern DinoSerial ser;
extern Sys sys;
extern Led led;
extern GnssComm gnss;

void system_BasicTrackerUploadLoop(int sec_between_uploads)
{
    for(;;) {
        wait(.1);

        gnss.powerOnEva();
        gnss.silenceAllInitialNmea();

        while(gnss.ubx_PollPvtForFix() != SUCCESS)
        {
            wait(5);
            led.blinkRedLed(.1);
        }

        wait(3);

        DSen sen;

        sen.setCoordinates(gnss.getDlon(), gnss.getDlat());
        
        string dsenstr = sen.buildAndGetSenStr();

        pass_string_to_ehs6(dsenstr.c_str(), dsenstr.length());

        gnss.powerOffEva();

        wait(.1);
        ser.selectPcSerial();
        ser.printf("All set! \r\n");
        wait(.1);

        led.blinkGreenLed(sec_between_uploads);
    }
}


void system_FakeTrackerUploadLoop(int sec_between_uploads)
{
    for(;;) {
        wait(.1);

        DSen sen;

        sen.setCoordinates(76.6767, 88.6666);
        
        string dsenstr = sen.buildAndGetSenStr();

        pass_string_to_ehs6(dsenstr.c_str(), dsenstr.length());

        wait(.1);
        ser.selectPcSerial();
        ser.printf("All set! \r\n");
        wait(.1);

        led.blinkGreenLed(sec_between_uploads);
    }
}