#ifndef TESTS_H
#define TESTS_H

#include "sd-card-tests.h"
#include "led-tests.h"
#include "ble-tests.h"
#include "i2c-tests.h"
#include "eeprom-tests.h"
#include "ubx-tests.h"
#include "uart-tests.h"
#include "lz-tests.h"
#include "ehs6-tests.h"
#include "am1805-tests.h"
#include "system-tests.h"
#include "Si7006-tests.h"

#endif  /* TESTS_H */