#include "mbed.h"
#include "dino5.h"
#include "uart-tests.h"

extern DinoSerial ser;

void uart_LoopbackTest(void) 
{
    ser.selectPcSerial();
    ser.printf("Starting UART loopback test...\r\n");

    for(;;) {
        if(ser.readable()) ser.putc(ser.getc());
    }
}

void uart_BufferedSerialResponseWindowTest(int window_ms)
{
    int i=0;
    ser.selectPcSerial();
    ser.printf("Opening %dms input window in...\r\n", window_ms);
    wait(1);
    ser.printf("3...\r\n");
    wait(1);
    ser.printf("2...\r\n");
    wait(1);
    ser.printf("1...\r\n");
    wait(1);
    ser.flushBuffer();
    while (i < window_ms) {
        wait(.001);
        i++;
        if(ser.readable()) ser.putc(ser.getc());
    }
    ser.printf("\r\nTest Complete.\r\n");
}