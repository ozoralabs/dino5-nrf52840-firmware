#ifndef UART_TESTS_H
#define UART_TESTS_H

void uart_LoopbackTest(void);
void uart_BufferedSerialResponseWindowTest(int window_ms);

#endif  /* UART_TESTS_H */