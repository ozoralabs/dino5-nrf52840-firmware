#include "mbed.h"
#include "dino5.h"
#include "ubx-tests.h"

extern DinoSerial ser;
extern DigitalOut eva_en;

const char silence_nmea[11] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x00, 0x00, 0xFA, 0x0F};

void ubx_BasicCommTest(void)
{
    char response_buffer[200];
    int expected_length = 11; // This can be determined for each command you want to run from the dreaded document.
    int i;

    wait(1);

    ser.selectPcSerial();
    ser.printf("Starting.\r\n");

    wait(.2);

    ser.sendEvaCmdAndGetReply(response_buffer, expected_length, silence_nmea, 10, 100); 
    // Again, we can't use something like 'strlen(ubx_command_4)' for the length. We have to use 11
    // This is cuz 'strlen' will get confused and stop counting at the first 0x00 character.
    wait(.2);
    ser.selectPcSerial();
    ser.printf("Got response: \r\n");
    wait(.2);
    for(i=0;i<expected_length;i++) {
        ser.printf("%X ", response_buffer[i]);
    }
    wait(.2);
}

void power_on_eva(void) {
    eva_en = 1;
    wait(0.1);
}

void power_off_eva(void) {
    eva_en = 0;
    wait(0.1);
}