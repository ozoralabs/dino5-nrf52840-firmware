#ifndef UBX_TESTS_H
#define UBX_TESTS_H

#include "dino5.h"

void ubx_BasicCommTest(void);
void power_on_eva(void);
void power_off_eva(void);

#endif  /* UBX_TESTS_H */